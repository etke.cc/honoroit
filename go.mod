module gitlab.com/etke.cc/honoroit

go 1.21.0

require (
	github.com/VictoriaMetrics/metrics v1.35.1
	github.com/archdx/zerolog-sentry v1.8.4
	github.com/dustin/go-humanize v1.0.1
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/labstack/echo/v4 v4.12.0
	github.com/lib/pq v1.10.9
	github.com/mileusna/crontab v1.2.0
	github.com/nixys/nxs-go-redmine/v5 v5.1.0
	github.com/rs/zerolog v1.33.0
	github.com/stretchr/testify v1.9.0
	github.com/ziflex/lecho/v3 v3.7.0
	gitlab.com/etke.cc/go/echo-basic-auth v1.1.0
	gitlab.com/etke.cc/go/env v1.2.0
	gitlab.com/etke.cc/go/healthchecks/v2 v2.2.0
	gitlab.com/etke.cc/go/mxidwc v1.0.0
	gitlab.com/etke.cc/go/psd v1.1.2
	gitlab.com/etke.cc/go/redmine v0.0.0-20240723150916-eb5794b51f51
	gitlab.com/etke.cc/linkpearl v0.0.0-20240716084747-f2a547f02d54
	golang.org/x/exp v0.0.0-20240719175910-8a7402abbf56
	maunium.net/go/mautrix v0.19.0
	modernc.org/sqlite v1.31.1
)

require (
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/getsentry/sentry-go v0.28.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/tidwall/gjson v1.17.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fastrand v1.1.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/valyala/histogram v1.2.0 // indirect
	github.com/yuin/goldmark v1.7.4 // indirect
	go.mau.fi/util v0.6.0 // indirect
	golang.org/x/crypto v0.25.0 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	modernc.org/gc/v3 v3.0.0-20240722195230-4a140ff9c08e // indirect
	modernc.org/libc v1.55.3 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
