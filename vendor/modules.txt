# github.com/VictoriaMetrics/metrics v1.35.1
## explicit; go 1.17
github.com/VictoriaMetrics/metrics
# github.com/archdx/zerolog-sentry v1.8.4
## explicit; go 1.20
github.com/archdx/zerolog-sentry
# github.com/buger/jsonparser v1.1.1
## explicit; go 1.13
github.com/buger/jsonparser
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/dustin/go-humanize v1.0.1
## explicit; go 1.16
github.com/dustin/go-humanize
# github.com/getsentry/sentry-go v0.28.1
## explicit; go 1.18
github.com/getsentry/sentry-go
github.com/getsentry/sentry-go/internal/debug
github.com/getsentry/sentry-go/internal/otel/baggage
github.com/getsentry/sentry-go/internal/otel/baggage/internal/baggage
github.com/getsentry/sentry-go/internal/ratelimit
github.com/getsentry/sentry-go/internal/traceparser
# github.com/golang-jwt/jwt v3.2.2+incompatible
## explicit
github.com/golang-jwt/jwt
# github.com/google/uuid v1.6.0
## explicit
github.com/google/uuid
# github.com/hashicorp/golang-lru/v2 v2.0.7
## explicit; go 1.18
github.com/hashicorp/golang-lru/v2
github.com/hashicorp/golang-lru/v2/internal
github.com/hashicorp/golang-lru/v2/simplelru
# github.com/kr/pretty v0.1.0
## explicit
# github.com/labstack/echo/v4 v4.12.0
## explicit; go 1.18
github.com/labstack/echo/v4
github.com/labstack/echo/v4/middleware
# github.com/labstack/gommon v0.4.2
## explicit; go 1.18
github.com/labstack/gommon/bytes
github.com/labstack/gommon/color
github.com/labstack/gommon/log
# github.com/lib/pq v1.10.9
## explicit; go 1.13
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.20
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/mileusna/crontab v1.2.0
## explicit; go 1.14
github.com/mileusna/crontab
# github.com/mitchellh/mapstructure v1.5.0
## explicit; go 1.14
github.com/mitchellh/mapstructure
# github.com/ncruces/go-strftime v0.1.9
## explicit; go 1.17
github.com/ncruces/go-strftime
# github.com/nixys/nxs-go-redmine/v5 v5.1.0
## explicit; go 1.21
github.com/nixys/nxs-go-redmine/v5
github.com/nixys/nxs-go-redmine/v5/mimereader
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec
## explicit; go 1.12
github.com/remyoudompheng/bigfft
# github.com/rs/zerolog v1.33.0
## explicit; go 1.15
github.com/rs/zerolog
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
github.com/rs/zerolog/log
# github.com/stretchr/testify v1.9.0
## explicit; go 1.17
github.com/stretchr/testify/assert
github.com/stretchr/testify/require
github.com/stretchr/testify/suite
# github.com/tidwall/gjson v1.17.1
## explicit; go 1.12
github.com/tidwall/gjson
# github.com/tidwall/match v1.1.1
## explicit; go 1.15
github.com/tidwall/match
# github.com/tidwall/pretty v1.2.1
## explicit; go 1.16
github.com/tidwall/pretty
# github.com/tidwall/sjson v1.2.5
## explicit; go 1.14
github.com/tidwall/sjson
# github.com/valyala/bytebufferpool v1.0.0
## explicit
github.com/valyala/bytebufferpool
# github.com/valyala/fastrand v1.1.0
## explicit
github.com/valyala/fastrand
# github.com/valyala/fasttemplate v1.2.2
## explicit; go 1.12
github.com/valyala/fasttemplate
# github.com/valyala/histogram v1.2.0
## explicit; go 1.12
github.com/valyala/histogram
# github.com/yuin/goldmark v1.7.4
## explicit; go 1.19
github.com/yuin/goldmark
github.com/yuin/goldmark/ast
github.com/yuin/goldmark/extension
github.com/yuin/goldmark/extension/ast
github.com/yuin/goldmark/parser
github.com/yuin/goldmark/renderer
github.com/yuin/goldmark/renderer/html
github.com/yuin/goldmark/text
github.com/yuin/goldmark/util
# github.com/ziflex/lecho/v3 v3.7.0
## explicit; go 1.17
github.com/ziflex/lecho/v3
# gitlab.com/etke.cc/go/echo-basic-auth v1.1.0
## explicit; go 1.21.0
gitlab.com/etke.cc/go/echo-basic-auth
# gitlab.com/etke.cc/go/env v1.2.0
## explicit; go 1.18
gitlab.com/etke.cc/go/env
gitlab.com/etke.cc/go/env/dotenv
# gitlab.com/etke.cc/go/healthchecks/v2 v2.2.0
## explicit; go 1.18
gitlab.com/etke.cc/go/healthchecks/v2
# gitlab.com/etke.cc/go/mxidwc v1.0.0
## explicit; go 1.19
gitlab.com/etke.cc/go/mxidwc
# gitlab.com/etke.cc/go/psd v1.1.2
## explicit; go 1.21.0
gitlab.com/etke.cc/go/psd
# gitlab.com/etke.cc/go/redmine v0.0.0-20240723150916-eb5794b51f51
## explicit; go 1.21
gitlab.com/etke.cc/go/redmine
# gitlab.com/etke.cc/linkpearl v0.0.0-20240716084747-f2a547f02d54
## explicit; go 1.21
gitlab.com/etke.cc/linkpearl
# go.mau.fi/util v0.6.0
## explicit; go 1.21
go.mau.fi/util/base58
go.mau.fi/util/dbutil
go.mau.fi/util/exerrors
go.mau.fi/util/exgjson
go.mau.fi/util/exzerolog
go.mau.fi/util/jsonbytes
go.mau.fi/util/jsontime
go.mau.fi/util/ptr
go.mau.fi/util/random
go.mau.fi/util/retryafter
# golang.org/x/crypto v0.25.0
## explicit; go 1.20
golang.org/x/crypto/acme
golang.org/x/crypto/acme/autocert
golang.org/x/crypto/curve25519
golang.org/x/crypto/hkdf
golang.org/x/crypto/pbkdf2
# golang.org/x/exp v0.0.0-20240719175910-8a7402abbf56
## explicit; go 1.20
golang.org/x/exp/constraints
golang.org/x/exp/maps
golang.org/x/exp/slices
# golang.org/x/net v0.27.0
## explicit; go 1.18
golang.org/x/net/html
golang.org/x/net/html/atom
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/h2c
golang.org/x/net/http2/hpack
golang.org/x/net/idna
# golang.org/x/sys v0.22.0
## explicit; go 1.18
golang.org/x/sys/execabs
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.16.0
## explicit; go 1.18
golang.org/x/text/cases
golang.org/x/text/internal
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# golang.org/x/time v0.5.0
## explicit; go 1.18
golang.org/x/time/rate
# gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15
## explicit
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
# maunium.net/go/mautrix v0.19.0
## explicit; go 1.21
maunium.net/go/mautrix
maunium.net/go/mautrix/crypto
maunium.net/go/mautrix/crypto/aescbc
maunium.net/go/mautrix/crypto/attachment
maunium.net/go/mautrix/crypto/backup
maunium.net/go/mautrix/crypto/canonicaljson
maunium.net/go/mautrix/crypto/cryptohelper
maunium.net/go/mautrix/crypto/goolm
maunium.net/go/mautrix/crypto/goolm/account
maunium.net/go/mautrix/crypto/goolm/cipher
maunium.net/go/mautrix/crypto/goolm/crypto
maunium.net/go/mautrix/crypto/goolm/libolmpickle
maunium.net/go/mautrix/crypto/goolm/megolm
maunium.net/go/mautrix/crypto/goolm/message
maunium.net/go/mautrix/crypto/goolm/olm
maunium.net/go/mautrix/crypto/goolm/pk
maunium.net/go/mautrix/crypto/goolm/session
maunium.net/go/mautrix/crypto/goolm/utilities
maunium.net/go/mautrix/crypto/olm
maunium.net/go/mautrix/crypto/pkcs7
maunium.net/go/mautrix/crypto/signatures
maunium.net/go/mautrix/crypto/sql_store_upgrade
maunium.net/go/mautrix/crypto/ssss
maunium.net/go/mautrix/crypto/utils
maunium.net/go/mautrix/event
maunium.net/go/mautrix/format
maunium.net/go/mautrix/format/mdext
maunium.net/go/mautrix/id
maunium.net/go/mautrix/pushrules
maunium.net/go/mautrix/pushrules/glob
maunium.net/go/mautrix/sqlstatestore
# modernc.org/gc/v3 v3.0.0-20240722195230-4a140ff9c08e
## explicit; go 1.20
modernc.org/gc/v3
# modernc.org/libc v1.55.3
## explicit; go 1.20
modernc.org/libc
modernc.org/libc/errno
modernc.org/libc/fcntl
modernc.org/libc/fts
modernc.org/libc/grp
modernc.org/libc/honnef.co/go/netdb
modernc.org/libc/langinfo
modernc.org/libc/limits
modernc.org/libc/netdb
modernc.org/libc/netinet/in
modernc.org/libc/poll
modernc.org/libc/pthread
modernc.org/libc/pwd
modernc.org/libc/signal
modernc.org/libc/stdio
modernc.org/libc/stdlib
modernc.org/libc/sys/socket
modernc.org/libc/sys/stat
modernc.org/libc/sys/types
modernc.org/libc/termios
modernc.org/libc/time
modernc.org/libc/unistd
modernc.org/libc/utime
modernc.org/libc/uuid
modernc.org/libc/uuid/uuid
modernc.org/libc/wctype
# modernc.org/mathutil v1.6.0
## explicit; go 1.18
modernc.org/mathutil
# modernc.org/memory v1.8.0
## explicit; go 1.18
modernc.org/memory
# modernc.org/sqlite v1.31.1
## explicit; go 1.20
modernc.org/sqlite
modernc.org/sqlite/lib
# modernc.org/strutil v1.2.0
## explicit; go 1.18
modernc.org/strutil
# modernc.org/token v1.1.0
## explicit
modernc.org/token
